from licenses import apache, gplv2

license_list = {
    "apache": apache.license_info,
    "gplv2": gplv2.license_info
}