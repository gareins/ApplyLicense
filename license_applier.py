#!/usr/bin/python3

# ======================= license_applier.py =======================
# 
# This source file is part of the LicenceApplier open source project
# Copyright (c) 2016 Ozbolt Menegatti
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Licensed under Apache License v2.0
# 
# ==================================================================


import argparse
from datetime import datetime
import math
import re
import os

from licenses import license_list
skip_folders = [".git", ".idea", ".svn", ".hg"]


def main():
    year = datetime.now().year

    parser = argparse.ArgumentParser(description=("Script, that adds Apache license to project and its source files. " +
                                                  "Licences to choose from: [{}]. " +
                                                  "Some directories are automatically excluded: [{}].")
                                     .format(", ".join(license_list.keys()), ", ".join(skip_folders)))

    parser.add_argument("-l", metavar="license", choices=list(license_list.keys()), required=True, help="Project license")
    parser.add_argument("-a", metavar="author", type=str, required=True, help="Author full name")
    parser.add_argument("-n", metavar="project_name", type=str, required=True, help="Project name")
    parser.add_argument("-d", metavar="dir", type=str, default=".", required=False, help="Project root directory, default './'")
    parser.add_argument("-y", metavar="year", type=int, default=year, required=False, help="Year in which this project was developed")
    parser.add_argument("--ext", metavar="extension", type=str, required=False, action="append", help="Only parse these extensions.")
    parser.add_argument("--exc", metavar="exclude_dirs", type=str, required=False, action="append", help="Directories to exclude from appending licences to.")
    parser.add_argument("--dry", action="store_true", help="Perform a run without actually changing the files")

    args = parser.parse_args()
    license_header, license_file = license_list[args.l]
    exclude_dirs, extension = args.exc, args.ext

    exclude_dirs = skip_folders if exclude_dirs is None else skip_folders + exclude_dirs
    exclude_dirs = [x + "/" if not x.endswith("/") else x for x in exclude_dirs]

    if "LICENSE" not in os.listdir(args.d):
        print("Adding LICENSE file")
        with open("LICENSE", "w") as fp:
            fp.write(license_file)

    for root, _, files in os.walk(args.d):
        folder = root[len(args.d) + 1:] + "/"

        to_continue = False
        for dir in exclude_dirs:
            if folder.startswith(dir):
                if len(folder) == len(dir):
                    print("{:60} : Skipping folder".format(folder))
                to_continue = True
                break

        if to_continue:
            continue

        for fname in files:
            f_full_name = root + "/" + fname
            f_local_name = os.path.basename(fname)
            f_extension = os.path.splitext(fname)[1][1:]

            if extension is not None and f_extension not in extension:
                print("{:60} : Skipping file".format(f_full_name))
                continue

            print("{:60}".format(f_full_name), end=" : ")

            if f_extension not in SPECIALIZED_HEADERS:
                print("Unknown extension")
                continue

            new_header = render_header(license_header, year=args.y, projname=args.n,
                                       author=args.a, fname=f_local_name, ext=f_extension)

            err = header_already_exists(f_full_name, new_header)
            if len(err) > 0:
                print(err)
                continue

            print("Adding license")
            if args.dry:
                continue

            with open(f_full_name, "r") as fp:
                filecontents = fp.read()

            with open(f_full_name, "w") as fp:
                if filecontents.startswith("#!"):
                    hashbang = filecontents.find("\n")
                    fp.write(filecontents[0:hashbang] + "\n\n")
                    filecontents = filecontents[hashbang:]

                fp.write(new_header)
                fp.write("\n")
                fp.write(filecontents)


def header_already_exists(fname, new_header):
    # location of matching source header in SOURCE_HEADER and source file
    CHECK_HEADER_MATCH = (5, 16)

    new_lines = [line.strip() for line in new_header.split("\n")[CHECK_HEADER_MATCH[0]:CHECK_HEADER_MATCH[1]]]
    with open(fname, "r") as fp:
        old_lines = [line.strip() for line in fp.read().split("\n")]
        if len(old_lines) < CHECK_HEADER_MATCH[1]:
            return ""

    new_lines = [line for line in new_lines if len(line) > 5]
    old_lines = [line for line in old_lines if len(line) > 5]

    num = 0
    for line in new_lines:
        if line in old_lines:
            num += 1

    if num == len(new_lines):
        return "License already exists"
    if num > len(new_lines)/4:
        return "Detected a part of license. Abort"
    else:
        return ""


def render_header(header, **kwargs):
    template = header[:]
    subs = {}
    special_header = SPECIALIZED_HEADERS[kwargs["ext"]]

    template = special_header[2] + " " + special_header[0] + "\n" + \
               "\n".join([special_header[2] + " " + line for line in template.split("\n")]) + \
               "\n" + special_header[2] + " " + special_header[1]

    matches = re.finditer(r"\{\$([a-zA-Z0-9_\-]+)\s*(([a-zA-Z0-9_\-]+)\(([a-zA-Z0-9\-\_\,=\s]+)\))?\}", template)
    for f in matches:
        sub = template[f.regs[0][0]: f.regs[0][1]]
        name = template[f.regs[1][0]: f.regs[1][1]]
        pre, post = '', ''

        if name not in kwargs:
            raise Exception("Undefined template name: " + name)
        else:
            name = str(kwargs[name])

        if f.regs[2] != (-1, -1):
            fun = template[f.regs[3][0]: f.regs[3][1]]
            args = template[f.regs[4][0]: f.regs[4][1]]

            if fun not in ["fill_post", "fill_pre", "fill_both"]:
                raise Exception("Function " + fun + " is not defined")

            args = args.replace(' ', '').split(",")
            num, char = int(args[0]), args[1]

            if len(name) > num:
                raise Exception("Name too long for filling")

            len_to_add = int(math.ceil((num - len(name)) / len(args[1])))
            to_add = (len_to_add * char)[:len_to_add]

            if fun == "fill_post":
                post = " " + to_add
            elif fun == "fill_pre":
                pre = to_add + " "
            else:
                middle = int(math.floor(len(to_add)/2))
                pre, post = to_add[:middle] + " ", " " + to_add[middle:]

        subs[sub] = pre + name + post

    for _f, _t in subs.items():
        template = template.replace(_f, _t)

    return template


SPECIALIZED_HEADERS = {
    "cpp": (
        "--{$fname fill_both(60, -)}--",
        "------------------------------------------------------------------",
        "//"
    ),
    "py": (
        "=={$fname fill_both(60, =)}==",
        "==================================================================",
        "#"
    )
}


SAME_HEADERS = {
    "cpp": ["hpp", "c", "h"]
}


SPECIALIZED_HEADERS.update({
  new: SPECIALIZED_HEADERS[old] for old, unknown in SAME_HEADERS.items() for new in unknown
})


if __name__ == '__main__':
    main()

